﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class Presenter
    {
        private Model model;
        private View view;

        public Presenter(Model model, View view)
        {
            this.model = model;
            this.view = view;
            view.presenter = this;
        }

        public void StartGame(string FirstName, string SecondName)
        {
            model.FirstPlayerName = FirstName;
            model.SecondPlayerName = SecondName;
            UpdateView();
        }               
      
        public void SetClick(int x,int y)
        {
            model.SetCoords(x, y);
            if (model.GameEnd)
                WinBox();
            else if (model.Draw)
                DrawBox();

        }

        public string IconSet()
        {
            string a;
            if (model.Move)
            {
                a = "O";
                UpdateView();
            }
            else
            {
                a = "X";
                UpdateView();
            }
            return a;
        }

        private void UpdateView()
        {
            view.UpdateName(model.FirstPlayerName,model.SecondPlayerName,model.Move);
        }

        private void WinBox()
        {
            view.WinMassage(model.FirstPlayerName, model.SecondPlayerName, model.Move);
        }

        private void DrawBox()
        {
            view.DrawMassage();
        }

    }
}
