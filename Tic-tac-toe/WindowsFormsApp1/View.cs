﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class View : Form
    {
        public Presenter presenter { get; set; }

        public View()
        {
            InitializeComponent();
        }         

        public void UpdateName(string FirstName, string SecondName, bool move)
        {
            if (move)
                label1.Text = SecondName.ToString();
            else label1.Text = FirstName.ToString();        
        }

        public void WinMassage(string FirstName, string SecondName, bool move)
        {
            if (move)
                MessageBox.Show("Переміг "+ FirstName.ToString(), "Кінець!");
            else MessageBox.Show("Переміг " + SecondName.ToString(), "Кінець!");
            Application.Exit();
        }

        public void DrawMassage()
        {
            MessageBox.Show("Перемогла дружба ", "Нічия!");
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            presenter.StartGame(textBox1.Text, textBox2.Text);
            label3.Visible = false;
            label4.Visible = false;
            textBox1.Visible = false;
            textBox2.Visible = false;
            button1.Visible = false;

            button2.Visible = true;
            button3.Visible = true;
            button4.Visible = true;
            button5.Visible = true;
            button6.Visible = true;
            button7.Visible = true;
            button8.Visible = true;
            button9.Visible = true;
            button10.Visible = true;
            label1.Visible = true;
            label2.Visible = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {     
            button2.Enabled = false;
            presenter.SetClick(2, 2);
            button2.Text = presenter.IconSet().ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {          
            button3.Enabled = false;
            presenter.SetClick(2, 3);
            button3.Text = presenter.IconSet().ToString();
        }

        private void button4_Click(object sender, EventArgs e)
        {           
            button4.Enabled = false;
            presenter.SetClick(2, 4);
            button4.Text = presenter.IconSet().ToString();
        }

        private void button5_Click(object sender, EventArgs e)
        {          
            button5.Enabled = false;
            presenter.SetClick(3, 2);
            button5.Text = presenter.IconSet().ToString();
        }

        private void button6_Click(object sender, EventArgs e)
        {          
            button6.Enabled = false;
            presenter.SetClick(3, 3);
            button6.Text = presenter.IconSet().ToString();
        }

        private void button7_Click(object sender, EventArgs e)
        {            
            button7.Enabled = false;
            presenter.SetClick(3, 4);
            button7.Text = presenter.IconSet().ToString();
        }

        private void button8_Click(object sender, EventArgs e)
        {            
            button8.Enabled = false;
            presenter.SetClick(4, 2);
            button8.Text = presenter.IconSet().ToString();
        }

        private void button9_Click(object sender, EventArgs e)
        {           
            button9.Enabled = false;
            presenter.SetClick(4, 3);
            button9.Text = presenter.IconSet().ToString();
        }

        private void button10_Click(object sender, EventArgs e)
        {                       
            button10.Enabled = false;
            presenter.SetClick(4, 4);
            button10.Text = presenter.IconSet().ToString();
        }

       
    }
}
