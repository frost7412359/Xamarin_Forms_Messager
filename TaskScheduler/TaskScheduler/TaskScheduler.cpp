﻿// TaskScheduler.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <ctime>
#include <conio.h>
#include <iterator>
#include "sstream"
#include <vector>
#include <iomanip>
#include <string>

using namespace std;

class Task
{
private:
	string taskname;
	string taskdescription;
	int year;
	int month;
	int day;
public:
	Task(int year, int month, int day, string taskname, string taskdescription)
	{
		this->taskname = taskname;
		this->taskdescription = taskdescription;
		this->year = year;
		this->month = month;
		this->day = day;
	};	

	friend ostream& operator<<(ostream& os, const Task& ts) ;
	friend bool operator== (const Task &c1, const Task &c2);
};

ostream& operator<<(ostream& os, const Task& ts)
{
	os << left << setw(4) << ts.year<< "." << setw(2) <<ts.month << "." << setw(4) << ts.day << setw(20)
		<< ts.taskname << setw(30) << ts.taskdescription;
	return os;
}

bool operator== (const Task &c1, const Task &c2)
{
	return (c1.year == c2.year && c1.month == c2.month && c1.day == c2.day);
}

class TaskScheduler
{
private:
	vector<Task> task;
	int year, month, day;
	int this_year, this_month, this_day;
	string TaskName;
	string TaskDescription;


public:

	TaskScheduler()	{ };

	void AddTaskForTodayAndTommorow()
	{
		time_t rawtime;
		struct tm * timeinfo;
		time(&rawtime);
		timeinfo = localtime(&rawtime);

		this_year = timeinfo->tm_year + 1900;
		this_month = timeinfo->tm_mon + 1;
		this_day = timeinfo->tm_mday;

		task.push_back(Task(timeinfo->tm_year + 1900, timeinfo->tm_mon + 1, timeinfo->tm_mday, "Go to store", "Buy apple, potato,chery,bred"));
		task.push_back(Task(timeinfo->tm_year + 1900, timeinfo->tm_mon + 1, timeinfo->tm_mday + 1, "Call doctor", "45-65-8439"));
		
	}

	void AddTask()
	{   
		bool true_year = false;
		bool true_month = false;
		bool true_day = false;

		while (true_year != true)
		{
			cout << "Enter year: ";    cin >> year;
			if (year >= this_year)
				true_year = true;
			else cout << "Incorrect year, try again" << endl;
		}
		
		while (true_month != true)
		{
			cout << "Enter month: "; cin >> month;
			if (year == this_year)
			{
				if ((month < 13) && (month >= this_month))
					true_month = true;
				else cout << "Incorrect month, try again" << endl;
			}
			else
			{
				if (month < 13)
					true_month = true;
				else cout << "Incorrect month, try again" << endl;
			}

		}

		while (true_day != true)
		{
			cout << "Enter day: ";   cin >> day;
			if ((year == this_year)&& (month == this_month))
			{
				if ((day < 31) && (day >= this_day))
					true_day = true;
				else cout << "Incorrect day, try again" << endl;
			}
			else
			{
				if (day < 31)
					true_day = true;
				else cout << "Incorrect day, try again" << endl;
			}
			
			
		}
		
		cout << "Enter task name: "; cin >> TaskName;
		cout << "Enter task description: ";   cin >> TaskDescription;

		task.push_back(Task(year, month, day, TaskName, TaskDescription));
	}

	void ShowTask()
	{
		cout << "Year: ";    cin >> year;
		cout << "Month: "; cin >> month;
		cout << "Day: ";   cin >> day;
		cout << endl;
		cout << setw(12) << "Date" << setw(20) << "Task name" << setw(30) << "Task description";
		Task specific_task(year, month, day, "", "");
		for (unsigned i = 0; i < task.size(); i++)
			if (task[i] == specific_task)
			{
				cout << endl << task[i];
			}
		
	}

	void ShowTaskForTodayAndTommorow()
	{
		Task today(this_year, this_month, this_day, "", "");
		Task tomorrow(this_year, this_month, this_day+1, "", "");
		cout << "Task for today";
		for (unsigned i = 0; i < task.size(); i++)
			if (task[i] == today)
			{
				cout << endl << task[i];
			}

		cout << endl << "Task for tomorrow";
		for (unsigned i = 0; i < task.size(); i++)
			if (task[i] == tomorrow)
			{
				cout << endl << task[i];
			}
	}
};


int main()
{
	int count;
	bool exit=false;
	TaskScheduler task_scheduler;
	task_scheduler.AddTaskForTodayAndTommorow();
	
	while (exit != true)
	{
		task_scheduler.ShowTaskForTodayAndTommorow();
		cout << endl << endl << "Enter 1 to add a task for a specific date, enter 2 to view tasks for a specific date or enter 3 to exit: ";
		cin >> count;
		switch (count)
		{
		case 1:
		{
			task_scheduler.AddTask();
			cout << "Tasl added, press any key to continue";
			getch();
			system("CLS");
			break;
		}
		case 2:
		{
			task_scheduler.ShowTask();
			cout << endl << "Press any key to continue";
			getch();
			system("CLS");
			break;
		}
		case 3:
		{
			exit = true;
			system("CLS");
			break;
		}
		default: 
		{
			cout << "Incorrect input, press any key to try again";
			getch();
			system("CLS");
			break;
		}
		}

	}

	return 0;
}

