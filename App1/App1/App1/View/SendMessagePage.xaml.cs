﻿using App1.Helpers;
using App1.Model;
using App1.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SendMessagePage : ContentPage
    {
        private ApiServices _apiServices = new ApiServices();

        ObservableCollection<MessageModel> mes2 = new ObservableCollection<MessageModel>();

        private SignalR _signalr = new SignalR();

        public ListView ListView { get { return listView; } }

        public string ID;

        public SendMessagePage(string Name, string id)

        {
            InitializeComponent();
            this.ID = id;
            _signalr.Connection();
            this.Title = Name;
            Send_text_entry.Text = "";



            _signalr.OnMessageReceived += (message, username) => {
                mes2.Add(new MessageModel
                {
                    Name = message,
                    Message = username,
                    Margin = new Thickness(0, 10, 30, 0),
                    Color = "#faba95",
                    date = DateTime.Now
                });
            };

            listView.ItemsSource = mes2;
            Add_current_message();

        }

        private async void Add_current_message()
        {
           List<GetMessage> mes_list = await _apiServices.GetMessages(ID);
           
            foreach (GetMessage list in mes_list)
            {
                if (list.Owner_UserName == Settings.User_name)
                {
                    mes2.Add(new MessageModel
                    {
                        Name = list.Owner_UserName,
                        Message = list.text,
                        Margin = new Thickness(30, 10, 0, 0),
                        Color = "#f9ebac",
                        date = Convert.ToDateTime(list.created)
                    });
                }
                else
                {
                    mes2.Add(new MessageModel
                    {
                        Name = list.Owner_UserName,
                        Message = list.text,
                        Margin = new Thickness(0, 10, 30, 0),
                        Color = "#faba95",
                        date = Convert.ToDateTime(list.created)
                    });
                }
               
            }
        }

        public void Send(object sender, EventHandler e)
        {
            if (Send_text_entry.Text != "")
            {
                _signalr.Send(Send_text_entry.Text, ID);


                mes2.Add(new MessageModel
                {
                    Name = Settings.User_name,
                    Message = Send_text_entry.Text,
                    Margin = new Thickness(30, 10, 0, 0),
                    Color = "#f9ebac",
                    date = DateTime.Now
                });

                Send_text_entry.Text = "";
                
            }
            
        }
    }
}