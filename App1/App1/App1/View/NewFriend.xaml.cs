﻿using App1.Helpers;
using App1.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class NewFriend : ContentPage
	{
        private ApiServices _apiServices = new ApiServices();

        string id;

        public NewFriend(string slave_login, string first_name,string last_name,string from_city,string date_of_birth)
        {
            InitializeComponent();
            this.id = slave_login;
            this.Title = first_name + " " + last_name;
            this.login.Text = slave_login;
            this.First_Last_name.Text = first_name + " " + last_name;
           // this.City.Text = from_city;
           // this.Birth.Text = date_of_birth;

        }

        async void Add_to_Friend_btn(object sender, EventHandler e)
        {
            var isSuccess = await _apiServices.Friend_request(Settings.User_name, id);

            if (isSuccess)
            {
                await DisplayAlert("Alert", "Success", "Ok");
                Add_btn.IsEnabled = false;
            }

            else
                await DisplayAlert("Alert", "Bad", "Ok");
        }
    }
}