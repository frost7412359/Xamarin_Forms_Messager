﻿using App1.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MasterPage : ContentPage
    {
        public ListView ListView { get { return listView; } }
        public Label UserName { get { return userName; } }

        public MasterPage()
        {
            InitializeComponent();

            var masterPageItem = new List<MasterPageItem>();
            masterPageItem.Add(new MasterPageItem
            {
                Title = "Friends",
                IconSource = "ic_group_black_48dp.png",
                TargetType = typeof(UserFriends)
            });

            masterPageItem.Add(new MasterPageItem
            {
                Title = "Messages",
                IconSource = "ic_message_black_48dp.png",
                TargetType = typeof(MessagePage)
            });

            masterPageItem.Add(new MasterPageItem
            {
                Title = "Search new user",
                IconSource = "add_group1600.png",
                TargetType = typeof(SearchNewFriend)
            });
           
            masterPageItem.Add(new MasterPageItem
            {
                Title = "Settings",
                IconSource = "ic_settings_black_48dp.png",
                TargetType = typeof(Setting)
            });
         


            listView.ItemsSource = masterPageItem;

        }



    }
}