﻿using App1.Services;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewAccount : ContentPage
    {
        ApiServices _apiServices = new ApiServices();

        public NewAccount()
        {
            InitializeComponent();
        }
        

        private async void Btn_Create_Clicked(object sender, EventArgs e)
        {
         

            Spiner.IsRunning = true;

            if (CrossConnectivity.Current.IsConnected)
            {
                var isSuccess = await _apiServices.RegisterUser(Reg_email.Text, Reg_password.Text, ConfirmPassword.Text,
                     picker_.SelectedItem.ToString(), First_name.Text, Last_name.Text, City_picker.SelectedItem.ToString(),
                     Date_of_birth.Date.ToString("yyyy/MM/dd"));

                Spiner.IsRunning = false;

                if (isSuccess)
                    await DisplayAlert("Alert", "Success", "Ok");
                else
                    await DisplayAlert("Alert", "Bad", "Ok");
            }
            else
            {
                Spiner.IsRunning = false;
                await DisplayAlert("Oops", "No internet connection", "Ok");
            }
        }

        private void visibility_button_checkbox_Clicked(object sender, EventArgs e)
        {
            if (Reg_password.IsPassword)
            {
                visibility_button_checkbox.Image = "ic_check_box_black_24dp.png";
                Reg_password.IsPassword = false;
                ConfirmPassword.IsPassword = false;
            }
            else
            {
                visibility_button_checkbox.Image = "ic_check_box_outline_blank_black_24dp.png";
                Reg_password.IsPassword = true;
                ConfirmPassword.IsPassword = true;
            }
        }

       
    }
}