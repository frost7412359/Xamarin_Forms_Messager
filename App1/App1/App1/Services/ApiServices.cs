﻿using App1.Helpers;
using App1.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace App1.Services
{
    class ApiServices
    {
        public async Task<bool> RegisterUser(string email, string password, string confirmPassword, string sex, string first_name, string last_name, string from_city, string birth)
        {
            var client = new HttpClient();

            var model = new RegisterModel
            {
                FirstName = first_name,
                LastName = last_name,
                Email = email,
                Password = password,
                ConfirmPassword = confirmPassword,
                Birth = birth,
                Sex = sex,
                FromCity = from_city

            };

            var json = JsonConvert.SerializeObject(model);

            HttpContent content = new StringContent(json);

            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync("http://35.158.210.136/api/Account/Register", content);

            return response.IsSuccessStatusCode;
        }


        public async Task<bool> LoginAsync(string username, string password)
        {

            var keyvalue = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("username",username),
                new KeyValuePair<string, string>("password", password),
                new KeyValuePair<string, string>("grant_type", "password")

            };

            var request = new HttpRequestMessage(HttpMethod.Post, "http://35.158.210.136/token");

            request.Content = new FormUrlEncodedContent(keyvalue);

            var handler = new HttpClientHandler();

            var client = new HttpClient(handler);

            CookieContainer cookies = new CookieContainer();

            handler.CookieContainer = new System.Net.CookieContainer();
            
            var response = await client.SendAsync(request);

            cookies = handler.CookieContainer;

            var jwt = await response.Content.ReadAsStringAsync();

            JObject jwtDynamic = JsonConvert.DeserializeObject<dynamic>(jwt);

            var accessToken = jwtDynamic.Value<string>("access_token");

            Settings.accessToken = accessToken;

            var user_id = jwtDynamic.Value<string>("user_id");

            Settings.user_id = user_id;

            return response.IsSuccessStatusCode;
        }


        public async Task<List<FriendModel>> GetFriends(string master_login)
        {
            var client = new HttpClient();

            client.DefaultRequestHeaders.Add("master_login", master_login);

            var json = await client.GetStringAsync("http://35.158.210.136/api/Friends");

            var friends = JsonConvert.DeserializeObject<List<FriendModel>>(json);

            return friends;
        }


        public async Task<List<FriendModel>> Get_Friend_request(string type)
        {
            var client = new HttpClient();

            client.DefaultRequestHeaders.Add("master_login", Settings.User_name);

            var json = await client.GetStringAsync("http://35.158.210.136/api/Friends?direction=" + type);

            var friends = JsonConvert.DeserializeObject<List<FriendModel>>(json);

            return friends;
        }


        public async Task<List<NewUser>> SearchUser(string login)
        {
            var client = new HttpClient();

            client.DefaultRequestHeaders.Add("seed", login);

            var json = await client.GetStringAsync("http://35.158.210.136/api/user");

            var user = JsonConvert.DeserializeObject<List<NewUser>>(json);

            return user;
        }


        public async Task<bool> Friend_request(string master_login, string slave_login)
        {
            var request = new HttpRequestMessage(HttpMethod.Put, "http://35.158.210.136/api/Friends");

            request.Headers.Add("master_login", master_login);
            request.Headers.Add("slave_login", slave_login);

            var client = new HttpClient();

            var response = await client.SendAsync(request);

            return response.IsSuccessStatusCode;
        }
        

        public async Task<bool> Type_friend_request(string master_loginn, string slave_loginn, string typen)
        {

            var request = new HttpRequestMessage(HttpMethod.Post, "http://35.158.210.136/api/Friends");

            request.Headers.Add("master_login", master_loginn);
            request.Headers.Add("slave_login", slave_loginn);
            request.Headers.Add("type", typen);

            var client = new HttpClient();

            var response = await client.SendAsync(request);

            return response.IsSuccessStatusCode;
        }

        public async Task<CookieContainer> Cookies()
        {

            var keyvalue = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("username",Settings.User_name),
                new KeyValuePair<string, string>("password", Settings.Password_),
                new KeyValuePair<string, string>("grant_type", "password")

            };

            var request = new HttpRequestMessage(HttpMethod.Post, "http://35.158.210.136/token");

            request.Content = new FormUrlEncodedContent(keyvalue);

            var handler = new HttpClientHandler();

            var client = new HttpClient(handler);

            CookieContainer cookies = new CookieContainer();

            handler.CookieContainer = new System.Net.CookieContainer();

            var response = await client.SendAsync(request);

            cookies = handler.CookieContainer;
            
            return cookies;
        }

        public async Task<bool> Delete_friend(string master_login, string slave_login)
        {
            var request = new HttpRequestMessage(HttpMethod.Delete, "http://35.158.210.136/api/Friends");

            request.Headers.Add("master_login", master_login);
            request.Headers.Add("slave_login", slave_login);

            var client = new HttpClient();

            var response = await client.SendAsync(request);

            return response.IsSuccessStatusCode;

            

        }

        public async Task<List<GetMessage>> GetMessages(string slave_login)
        {
            var client = new HttpClient();

            var json = await client.GetStringAsync("http://35.158.210.136/api/messages?userId="+Settings.user_id+"&destinationUserId="+slave_login);

            var message = JsonConvert.DeserializeObject<List<GetMessage>>(json);

            return message;
        }

        public async Task<bool> Changes_data (string first_name, string Last_name, string city, string phone)
        {

            var request = new HttpRequestMessage(HttpMethod.Put, "http://35.158.210.136/api/user?token_type=bearer&amp;access_token="+Settings.accessToken);

            request.Headers.Add("firstName", first_name);
            request.Headers.Add("city", city);
            request.Headers.Add("phone", phone);
            request.Headers.Add("lastName", Last_name);

            var client = new HttpClient();

            var response = await client.SendAsync(request);

            return response.IsSuccessStatusCode;
        }

    }

}
