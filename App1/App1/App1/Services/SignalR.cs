﻿using Microsoft.AspNet.SignalR.Client;
using System;

using System.ComponentModel;


namespace App1.Services
{
    public class SignalR
    {
        ApiServices _apiServices = new ApiServices();

        public HubConnection _connection;

        public IHubProxy _proxy;

        public delegate void MessageReceived(string username, string message);
        public event MessageReceived OnMessageReceived;

  

        public string n1, n2;

        public SignalR()
        { }

        public void Connection()
        {
            _connection = new HubConnection("http://35.158.210.136/");
            _proxy = _connection.CreateHubProxy("chatHub");

            Get_message();

            StartChat();
        }

        public async void StartChat()
        {
            _connection.CookieContainer = await _apiServices.Cookies();

            await _connection.Start();
        }

        public void Get_message()
        {
            _proxy.On<String, String>("OnMessage", (userName, message) =>
            {
                OnMessageReceived?.Invoke(userName, message);
              
            });
        }

        public void Send(string text, string id)
        {
            _proxy.Invoke("SendMessage", text, id);
        }

     
    }
}
