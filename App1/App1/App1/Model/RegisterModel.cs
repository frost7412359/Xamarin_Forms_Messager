﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1.Model
{
    class RegisterModel
    {
        public string Email { get; set; }

        public string Password { get; set; }

        public string ConfirmPassword { get; set; }

        public string Sex { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FromCity { get; set; }

        public string Birth { get; set; }

    }
}
