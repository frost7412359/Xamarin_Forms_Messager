﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1.Model
{
    class MessageModel
    {
        public string Name { get; set; }
        public string Message { get; set; }
        public Xamarin.Forms.Thickness Margin { get; set; }
        public string Color { get; set; }
        public DateTime date { get; set; }
    }
}
